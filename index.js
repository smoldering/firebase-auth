//jshint esversion:6

const express = require('express');
const dotenv  = require('dotenv');
dotenv.config();
const app = express();

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/home.html');
});

app.get('/profile', (req, res) => {
    res.sendFile(__dirname + '/profile.html');
});

app.get('/login', (req, res) => {
    res.sendFile(__dirname + '/login.html');
});

app.get('/signup', (req, res) => {
    res.sendFile(__dirname + '/signup.html');
});

app.use(express.static('public'));

app.listen(3000, () => {
    console.log(`Server started on 3000`);
});



