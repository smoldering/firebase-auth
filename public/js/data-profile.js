     //jshint esversion:6

     //Read the user's custom info from the Firebase database if it exists
     const uid = firebase.auth().currentUser.uid;
     //
     const ref = firebase.database().ref().child('users/' + uid);

     const getfirstName = function (snapshot) {
         var firstName;
         //Get the user's first name if it exists
         if (snapshot.val().firstName == undefined || snapshot.val().firstName == null) {
             firstName = "";
         } else {
             firstName = snapshot.val().firstName;
         }
         return firstName;
     };

     const getlastName = function (snapshot) {
         var lastName;
         //Get the user's last name if it exists
         if (snapshot.val().lastName == undefined || snapshot.val().lastName == null) {
             lastName = "";
         } else {
             lastName = snapshot.val().lastName;
         }
         return lastName;
     }

     const getphoneNumber = function (snapshot) {
         var phoneNumber;
         //Get the user's phone number if it exists
         if (snapshot.val().phoneNumber == undefined || snapshot.val().phoneNumber == null) {
             phoneNumber = "";
         } else {
             phoneNumber = snapshot.val().phoneNumber;
         }
         return phoneNumber;
     };

     const getbio = function (snapshot) {
         var bio;
         //Get the user's bio if it exists
         if (snapshot.val().bio == undefined || snapshot.val().bio == null) {
             bio = "";
         } else {
             bio = snapshot.val().bio;
         }
         return bio;
     };

    firebase.database().ref().child('users/' + uid).once('value', function (snapshot) {
         if (snapshot.val() !== null) {
             ref.on("value", function (snapshot) {

                 //Pass the user's first name to the first-name input on the profile page
                 document.getElementById('first-name').value = getfirstName(snapshot);

                 //Pass the user's last name to the last-name input on the profile page
                 document.getElementById('last-name').value = getlastName(snapshot);

                 //Pass the user's phone number to the phone number input on the profile page
                 document.getElementById('phone-number').value = getphoneNumber(snapshot);

                 //Pass the user's bio to the phone bio input on the profile page
                 document.getElementById('bio').value = getbio(snapshot);
             }, function (error) {
                 console.log("Error: " + error);
             });
         }
     });


