//jshint esversion:6
// run the initApp() function as soon as the page loads
//Run the initApp() function as soon as the page loads
window.addEventListener('load', function () {
    initApp();
});

const getfirstName = function (snapshot) {
    var firstName;
    //Get the user's first name if it exists
    if (snapshot.val().firstName == undefined || snapshot.val().firstName == null) {
        firstName = "";
    } else {
        firstName = snapshot.val().firstName;
    }
    return firstName;
};

const getlastName = function (snapshot) {
    var lastName;
    //Get the user's last name if it exists
    if (snapshot.val().lastName == undefined || snapshot.val().lastName == null) {
        lastName = "";
    } else {
        lastName = snapshot.val().lastName;
    }
    return lastName;
}
const getphoneNumber = function (snapshot) {
    var phoneNumber;
    //Get the user's phone number if it exists
    if (snapshot.val().phoneNumber == undefined || snapshot.val().phoneNumber == null) {
        phoneNumber = "";
    } else {
        phoneNumber = snapshot.val().phoneNumber;
    }
    return phoneNumber;
};

const getbio = function (snapshot) {
    var bio;
    //Get the user's bio if it exists
    if (snapshot.val().bio == undefined || snapshot.val().bio == null) {
        bio = "";
    } else {
        bio = snapshot.val().bio;
    }
    return bio;
};

const firebaseDB = function (uid) {
    firebase.database().ref().child('users/' + uid).once('value', function (snapshot) {
        if (snapshot.val() !== null) {
            const ref = firebase.database().ref().child('users/' + uid);
            ref.on("value", function (snapshot) {

                //Pass the user's first name to the first-name input on the profile page
                document.getElementById('first-name').value = getfirstName(snapshot);

                //Pass the user's last name to the last-name input on the profile page
                document.getElementById('last-name').value = getlastName(snapshot);

                //Pass the user's phone number to the phone number input on the profile page
                document.getElementById('phone-number').value = getphoneNumber(snapshot);

                //Pass the user's bio to the phone bio input on the profile page
                document.getElementById('bio').value = getbio(snapshot);
            }, function (error) {
                    console.log("Error: " + error);
            });
        }
    });
}


// File name is 'space.jpg'
//var name = imageRef.name;
//To upload a file to Cloud Storage, you first create a reference
// to the full path of the file, including the file name.
// Create a child reference
//var imagesRef = storage.child('images/protected.png');

//var file = ... // use the Blob or File API
 
// Create file metadata including the content type
// var metadata = {
//     contentType: 'image/jpeg',
// };
// Upload the file and metadata
// imagesRef.put(file, metadata).then(function (snapshot) {
//     console.log('Uploaded a blob or file!');
// }).
// catch(function(error){
//     console.log('Something went wrong: ' + error);
// });

//Sets all the inputs on the Profile page
initApp = function () {
    firebase.auth().onAuthStateChanged(function (user) {
        var email;
        if (user) {
            // Get the authenticated user's email address
            if (user.email == null) {
                email = "";
            } else {
                email = user.email;
            }
            // Pass the email address to the email-address input on the Profile page
            document.getElementById('email-address').value = email;

            //Read the user's custom info from the Firebase database if it exists
            var uid = firebase.auth().currentUser.uid;
            //
            firebaseDB(uid);
        }
    })
}

//Allow the user to update their custom profile info
document.getElementById("btnUpdateProfile").addEventListener('click', function (event) {
    var firstName = document.getElementById('first-name').value;
    var lastName = document.getElementById('last-name').value;
    var phoneNumber = document.getElementById('phone-number').value;
    var bio = document.getElementById('bio').value;
    var uid = firebase.auth().currentUser.uid;
    firebase.database().ref('users/' + uid).update({
            uid: uid,
            firstName: firstName,
            lastName: lastName,
            phoneNumber: phoneNumber,
            bio: bio
        })
        //Trigger a success message if it works
        .then(function () {
            updateSuccess.innerText = "Profile successfully updated!";
            updateSuccess.style.display = 'block';
            console.log("Success! Profile updated.");
        })
        //Trigger an error message if it fails
        .catch(function (error) {
            var errorMessage = error.message;
            console.log("Whoops, something went wrong " + error);
            updateError.innerText = errorMessage;
            updateError.style.display = 'block';
        })
})